<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Form\OrderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class OrderController
 * @package App\Controller
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/order/new", name="order_new", methods={"POST"})
     */
    public function new(Request $request, RouterInterface $router): Response
    {
        if (!($user = $this->getUser())) {
            return $this->redirectToRoute('app_login');
        }
        $form = $this->createForm(OrderType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $entityManager = $this->getDoctrine()->getManager();
            /** @var Order $order */
            $order = $form->getData();
            $order->setUser($user);
            $cart = $request->cookies->get('cart');
            $cartProducts = json_decode($cart, true);
            $products = $entityManager->getRepository(Product::class)->findAllWithIdKeysByIds(array_unique($cartProducts));
            $price = 0;
            foreach ($cartProducts as $cartProduct) {
                /** @var Product $product */
                $product = $products[$cartProduct];
                $price += $product->getPrice();
                $order->addProduct($product);
                $product->setQuantity(($quantity = $product->getQuantity() - 1) < 0 ? 0 : $quantity);
                $entityManager->persist($product);
            }
            $order->setPrice($price);
            $entityManager->persist($order);
            $entityManager->flush();

            $response = new RedirectResponse($router->generate('home'));
            $response->headers->clearCookie('cart');
            $response->sendHeaders();

            return $response;
        }

        return $this->redirectToRoute('home');
    }
}
