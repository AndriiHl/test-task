<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $products = $entityManager
            ->getRepository(Product::class)
            ->findAllWithCategory();

        return $this->render('home/index.html.twig',
            [
                'products' => $products,
                'cartProducts' => json_decode($request->cookies->get('cart'), true) ?? []
            ]
        );
    }

    /**
     * @Route ("/categories", name="categories")
     */
    public function showCategory(EntityManagerInterface $entityManager): Response
    {
        $categories = $entityManager
            ->getRepository(Category::class)
            ->findAll();

        return $this->render('home/categories.html.twig',
            [
                'categories' => $categories
            ]
        );
    }
}
