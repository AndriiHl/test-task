<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\Product;
use App\Form\OrderType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class CartController
 * @package App\Controller
 */
class CartController extends AbstractController
{
    /** @var RouterInterface */
    protected $router;

    /** @var EntityManagerInterface */
    protected $entityManager;

    public function __construct(RouterInterface $router, EntityManagerInterface $entityManager)
    {
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/add-to-cart/{product}", name="add_to_cart")
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function addToCart(Product $product, Request $request): Response
    {
        $cart = $request->cookies->get('cart');
        $routeName = $this->routeName($request);
        if (!empty($cart)) {
            $cookie = new Cookie('cart', json_encode(array_merge(json_decode($cart, true), [$product->getId()])));
        } else {
            $cookie = new Cookie('cart', json_encode([$product->getId()]));
        }
        $response = new RedirectResponse($this->router->generate($routeName));
        $response->headers->setCookie($cookie);
        $response->sendHeaders();

        return $response;
    }

    /**
     * @Route("/cart/{product}/remove", name="remove_from_cart")
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function removeProductFromCart(Product $product, Request $request): Response
    {
        $cart = $request->cookies->get('cart');
        $routeName = $this->routeName($request);
        $response = new RedirectResponse($this->router->generate($routeName));
        $cartProducts = json_decode($cart, true);
        $cartProducts = array_values($cartProducts);
        $key = array_search($product->getId(), $cartProducts, true);

        if ($key || $key === 0) {
            unset($cartProducts[$key]);
            $cookie = new Cookie('cart', json_encode(array_values($cartProducts)));
            $response->headers->setCookie($cookie);
        }
        if (empty($cartProducts)) {
            $response->headers->clearCookie('cart');
        }
        $response->sendHeaders();

        return $response;
    }

    /**
     * @Route("/cart", name="show_cart")
     */
    public function index(Request $request)
    {
        $cart = $request->cookies->get('cart');
        $cartProducts = json_decode($cart, true);
        if (empty($cartProducts)) {
            return $this->redirectToRoute('home');
        }
        $products = $this->entityManager->getRepository(Product::class)->findBy(['id' => array_unique($cartProducts)]);
        $cartProductsFormatted = [];
        array_map(function ($item) use (&$cartProductsFormatted) {
            if (array_key_exists($item, $cartProductsFormatted)) {
                $cartProductsFormatted[$item]++;
            } else {
                $cartProductsFormatted[$item] = 1;
            }
        }, json_decode($cart, true));

        $order = new Order();
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        return $this->render('home/cart.html.twig',
            [
                'products' => $products,
                'cartProductsFormatted' => $cartProductsFormatted,
                'cartProducts' => json_decode($request->cookies->get('cart'), true) ?? [],
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function routeName(Request $request): string
    {
        $routeName = 'home';
        if ($request->headers->get('referer') === $this->generateUrl('show_cart', [], UrlGeneratorInterface::ABSOLUTE_URL)) {
            $routeName = 'show_cart';
        }

        return $routeName;
    }
}